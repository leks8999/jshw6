/* Ответы на теоретические вопросы:
1. Экранирование - это добавление спецсимволов перед строками или символами для их корректной интерпретации.
2. Обьявление функции (Fuction Declaration)
 function имя(параметры){
    тело функции
 }
   Функциональное выражение (Function Expression)
 let showAge = function () {
    тело}
3. Hoisting - это механизм, в котором переменные и объявления функций,
 передвигаются вверх своей области видимости перед тем, как код будет выполнен. неважно где были объявлены функция
  или переменные, все они передвигаются вверх своей области видимости, вне зависимости от того локальная она или же глобальная.
  Он передвигает только объявления функции или переменной, назначения переменным остаются на своих местах. */


function createNewUser() {
    let firstName = prompt('Введите имя');
    let lastName = prompt('Введите фамилию');
    let birthday = new Date((prompt('Введите дату в формате dd.mm.yyyy').split('.').reverse()));
    const newUser = {
        firstName,
        lastName,
        birthday: birthday.toLocaleDateString(),
        getLogin() {
            return firstName.toLowerCase()[0] + lastName.toLowerCase()
        },
        getAge() {
            let now = new Date();
            let today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); /* Текущяя дата */
            let dobnow = new Date(today.getFullYear(), birthday.getMonth(), birthday.getDate()); /* ДР в текущем году */
            let age = today.getFullYear() - birthday.getFullYear();
            if (today < dobnow) {
                age = age - 1;
            }
            return age
        },
        getPassword() {
            return firstName.toUpperCase()[0] + lastName.toLowerCase() + birthday.getFullYear()
        }
    }
    return newUser
}
let user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());
console.log(user);






